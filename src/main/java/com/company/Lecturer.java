package com.company;

public class Lecturer implements Role {

    @Override
    public void info() {
        System.out.println("I am a lecturer!");
    }
}
