package com.company;

public class RoleSystem {
    public Role getInstance(String s){
        if(s.equals("Knowledge")){
            return new Lecturer();
        }
        else if(s.equals("Creativity")){
            return new Student();
        }
        else{
            return new Administrator();
        }
    }
}
