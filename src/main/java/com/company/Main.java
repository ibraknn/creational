package com.company;

public class Main {

    public static void main(String[] args) {
        RoleSystem rs = new RoleSystem();
        Role r1 = rs.getInstance("Creativity");
        r1.info();

        Role r2 = rs.getInstance("Knowledge");
        r2.info();

        Role r3 = rs.getInstance("Blabla");
        r3.info();
    }
}
